<?php

function custom_admin_captcha_form(){
  $form = array();
  $form['google_captcha_site_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('Site key'),
    '#default_value' => variable_get('google_captcha_site_key', ''),
    '#description' => t('Use this key in the HTML code that you offer to your users.'),
    '#required' => true,
  );
  $form['google_captcha_secret_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('Secret key'),
    '#default_value' => variable_get('google_captcha_secret_key', ''),
    '#description' => t('Use this key to communication between your site and Google. Be careful not to disclose because it is a secret key.'),
    '#required' => true,
  );
  return system_settings_form($form);
}

/**
 * import document
 */
function custom_import_document_batch_generate($page, $dir, &$context){
  if (is_dir($dir.'/'.$page)) {
    $files = scandir($dir.'/'.$page);
    $docs = [];
    $images = [];
    $thumbs = [];
    $desc = '';
    $keywords = '';
    foreach ($files as $file){
      if( $file == '.' | $file == '..' ){
        continue;
      }
      if( !is_dir($dir.'/'.$page.'/'.$file) ){
        @chown($dir.'/'.$page.'/'.$file, 48);
        drupal_chmod('public://documents-upload/'.$page.'/'.$file, 0777);
      }

      $path_info = pathinfo($dir.'/'.$page.'/'.$file);

      if( is_dir($dir.'/'.$page.'/'.$file) ){
        /// duyet thu muc anh
        $images_file = scandir($dir.'/'.$page.'/'.$file);
        if( basename($dir.'/'.$page.'/'.$file) == 'thumb' ){
          usort($images_file, function($a, $b){
            if( (int)$a > (int)$b ) return 1;
            return -1;
          });
        }
        foreach ($images_file as $key => $image) {
          // @chown($dir.'/'.$page.'/'.$file.'/'.$image, 48);
          // drupal_chmod('public://documents-upload/'.$page.'/'.$file.'/'.$image, 0777);
          if( !is_dir($dir.'/'.$page.'/'.$file.'/'.$image) ){
            if( $file == 'bgimg' ){
              $images[] = $dir.'/'.$page.'/'.$file.'/'.$image;
            } elseif( $file == 'thumb' ){
              $thumbs[] = $dir.'/'.$page.'/'.$file.'/'.$image;
            }
          }
        }
      }
      elseif( $file != 'index.html' && $file != 'paging.html' && $path_info['extension'] == 'html' ){
        /// duyet file html
        $docs[] = $dir.'/'.$page.'/'.$file;
      }
      elseif( $file == $page.' Description.txt' ){
        /// duyet file description.txt
        $desc = file_get_contents($dir.'/'.$page.'/'.$file);
      }
      elseif( $file == $page.' Keyword.txt' ){
        /// duyet file description.txt
        $keywords = file_get_contents($dir.'/'.$page.'/'.$file);
      }
      elseif( $path_info['extension'] == 'pdf' ){
        /// duyet file description.txt
        // dsm($dir.'/'.$page.'/'.$file);
        $pdf = $dir.'/'.$page.'/'.$file;
      }
    }
    

    $nid = db_query('SELECT nid FROM {node} WHERE title = :mytitle', array(':mytitle' => $page))->fetchField();
    
    if( $nid ){
      $node = node_load($nid);
      $node->status = 1; /// published node
    }
    else{
      /////////// Create node Document
      $node = new stdClass(); // We create a new node object
      $node->type = "document"; // Or any other content type you want
      $node->title = $page;
      $node->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled.
      node_object_prepare($node); // Set some default values.
      $node->uid = 1; // Or any id you wish
      $node->status = 0; /// Unpublished node
    }

    if( !empty($desc) ) $node->metatags[LANGUAGE_NONE]['description']['value'] = $desc;
    if( !empty($keywords) ) $node->metatags[LANGUAGE_NONE]['keywords']['value'] = $keywords;
    $node->body[LANGUAGE_NONE][0] = ['value' => $desc, 'format' => 'full_html'];

    $document_dir = 'public://documents/'.$page;
    $document_thumbs = 'public://documents/'.$page.'/bgimg';
    $document_img = 'public://documents/'.$page.'/thumb';
    file_prepare_directory($document_dir, FILE_CREATE_DIRECTORY);
    file_prepare_directory($document_img, FILE_CREATE_DIRECTORY);
    file_prepare_directory($document_thumbs, FILE_CREATE_DIRECTORY);

    if( !empty($pdf) ){
      $node->field_file[LANGUAGE_NONE] = [];
      $file_path = drupal_realpath($pdf);
      $file = (object) array(
        'uid' => 1,  
        'uri' => $file_path,
        'filemime' => file_get_mimetype($file_path),
        'status' => 1,
        'display' => 1,
      );
      $file = file_copy($file, 'private://'.$page, FILE_EXISTS_REPLACE);
      $node->field_file[LANGUAGE_NONE][] = (array)$file;
    }

    if( count($images) ){
      $node->field_image[LANGUAGE_NONE] = [];
      foreach ($images as $key => $value) {
        $file_path = drupal_realpath($value); // Create a File object
        $file = (object) array(
          'uid' => 1,
          'uri' => $file_path,
          'filemime' => file_get_mimetype($file_path),
          'status' => 1,
          'display' => 1,
        );
        $file = file_copy($file, $document_thumbs, FILE_EXISTS_REPLACE); // Save the file to the root of the files directory. You can specify a subdirectory, for example, 'public://images'
         
        $node->field_image[LANGUAGE_NONE][] = (array)$file;
      }
    }

    if( count($thumbs) ){
      $node->field_thumbs[LANGUAGE_NONE] = [];
      foreach ($thumbs as $key => $value) {
        $file_path = drupal_realpath($value); // Create a File object
        $file = (object) array(
          'uid' => 1,
          'uri' => $file_path,
          'filemime' => file_get_mimetype($file_path),
          'status' => 1,
          'display' => 1,
        );
        $file = file_copy($file, $document_img, FILE_EXISTS_REPLACE); // Save the file to the root of the files directory. You can specify a subdirectory, for example, 'public://images'
         
        $node->field_thumbs[LANGUAGE_NONE][] = (array)$file;
      }
    }
    
    if( count($docs) ){
      $node->field_content[LANGUAGE_NONE] = [];
      foreach ($docs as $key => $value) {
        $file_path = drupal_realpath($value); // Create a File object
        $file = (object) array(
          'uid' => 1,
          'uri' => $file_path,
          'filemime' => file_get_mimetype($file_path),
          'status' => 1,
          'display' => 1,
        );
        $file = file_copy($file, $document_dir, FILE_EXISTS_REPLACE); // Save the file to the root of the files directory. You can specify a subdirectory, for example, 'public://images'
         
        $node->field_content[LANGUAGE_NONE][] = (array)$file;
      }
    }
    
    node_save($node);
    if( $node->nid > 0 ){
      //// Delete folder after import
      file_unmanaged_delete_recursive($dir.'/'.$page);
      
      watchdog('Import Document', 'The document "%page" was imported and the directory "%path" was deleted.', array(
        '%path' => '/documents-upload/'.$page,
        '%page' => $page
      ), WATCHDOG_NOTICE);
      if (!isset($context['results']['count'])) {
        $context['results']['count'] = 0;
      }
      $context['results']['count']++;
    }
  }
}

/**
 *
 */
function _filter_dom_doc_html($node = null){
  $page = !empty($_GET['page']) ? $_GET['page'] : 1;
  if($cached = cache_get('document_body:id-'.$node->nid.'-page-'.$page, 'cache'))  {
    $body = $cached->data;
  }
  if(empty($body)) {

    include(DRUPAL_ROOT.'/'. drupal_get_path('module', 'custom').'/vendor/simple_html_dom.php');
    $url = file_create_url($node->field_content['und'][$page-1]['uri']);
    $img = file_create_url($node->field_image['und'][$page-1]['uri']);
    $html = @file_get_html($url);
    $background = $html->find('body', 0)->getAttribute('background');

    if( !empty($background) ){
      $background = 'background: url(\''.$img.'\') no-repeat;';
      $html->find('body>div', 0)->class .= 'frame-wrap';
      $html->find('body>div', 0)->style .= $background;
    }
    $body = '<div class="frame-doc">'.(!empty($html->find('body', 0)->innertext) ? $html->find('body', 0)->innertext : $html->innertext). '</div>';
    cache_set('document_body:id-'.$node->nid.'-page-'.$page, $body, 'cache', time() + 60*60); //1 hour
  }
  
  return $body;
}

/**
 *
 */
function custom_import_document_batch_node_save($node){
  
}

/**
 * Render custom pager
 */
function _custom_pager($variables){
  $output = "";
  $element = 0;
  $items = isset($variables['items']) ? $variables['items'] : [];
  $tags = isset($variables['tags']) ? $variables['tags'] : []; // Text of first, previous, next, last
  $parameters = isset($variables['parameters']) ? $variables['parameters'] : [];
  $quantity = isset($variables['quantity']) ? $variables['quantity'] : 5; // middle around currrent page
  
  // Current is the page we are currently paged to.
  $pager_current = isset($variables['current']) ? $variables['current'] : 0;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // First is the first page listed by this pager piece (re quantity).
  $pager_first = $pager_current - $pager_middle + 1;
  // Last is the last page listed by this pager piece (re quantity).
  $pager_last = $pager_current + $quantity - $pager_middle;
  // Max is the maximum page number.
  $pager_max = isset($variables['total']) ? $variables['total'] : 1;

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }

  // End of generation loop preparation.
  // if( $pager_first > 1 ){
    $li_first = '<a' . drupal_attributes(['class' => ($pager_first == 1) ? 'disable' : '', 'href' => url($_GET['q']) , 'title' => t('Go to first page')]) . '><i class="fa fa-angle-double-left" aria-hidden="true"></i> ' . (isset($tags[1]) ? $tags[1] : t('first')) . '</a>';
  // }
  // if( $pager_current > 1 ){
    $li_previous = '<a' . drupal_attributes(['class' => ($pager_current == 1) ? 'disable' : '', 'href' => url($_GET['q'], ['query' => ['page' => $pager_current-1]]) , 'title' => t('Go to previous page')]) . '><i class="fa fa-angle-left" aria-hidden="true"></i> ' . (isset($tags[1]) ? $tags[1] : t('previous')) . '</a>';
  // }
  // if( $pager_current < $pager_max ){
    $li_next = '<a' . drupal_attributes(['class' => ($pager_current >= $pager_max) ? 'disable' : '', 'href' => url($_GET['q'], array('query' => ['page' => $pager_current+1])), 'title' => t('Go to next page')]) . '>' . (isset($tags[2]) ? $tags[2] : t('next')) . ' <i class="fa fa-angle-right" aria-hidden="true"></i></a>';
  // }
  // if( $pager_last < $pager_max ){
    $li_last = '<a' . drupal_attributes(['class' => ($pager_last >= $pager_max) ? 'disable' : '', 'href' => url($_GET['q'], ['query' => ['page' => $pager_max]]) , 'title' => t('Go to last page')]) . '>' . (isset($tags[1]) ? $tags[1] : t('last')) . ' <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>';
  // }

  if ($pager_max > 1) {

    // Only show "first" link if set on components' theme setting
    if (isset($li_first) && bootstrap_setting('pager_first_and_last')) {
      $items[] = array(
        'class' => array('pager-first'),
        'data' => $li_first,
      );
    }
    if (isset($li_previous)) {
      $items[] = array(
        'class' => array('prev'),
        'data' => $li_previous,
      );
    }
    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis', 'disabled'),
          'data' => '<span>…</span>',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => '<a' . drupal_attributes(['href' => url($_GET['q'], array('query' => ['page' => $i])), 'title' => t('Go to page @number', ['@number' => $i])]) . '>' . $i . '</a>'
            );
        }
        if ($i == $pager_current) {
          $items[] = array(
            // Add the active class.
            'class' => array('active'),
            'data' => "<span>$i</span>",
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => '<a' . drupal_attributes(['href' => url($_GET['q'], array('query' => ['page' => $i])), 'title' => t('Go to page @number', ['@number' => $i])]) . '>' . $i . '</a>'
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis', 'disabled'),
          'data' => '<span>…</span>',
        );
      }
    }
    // End generation.
    if (isset($li_next)) {
      $items[] = array(
        'class' => array('next'),
        'data' => $li_next,
      );
    }
    // // Only show "last" link if set on components' theme setting
    if (isset($li_last) && bootstrap_setting('pager_first_and_last')) {
      $items[] = array(
       'class' => array('pager-last'),
       'data' => $li_last,
      );
    }

    $build = array(
      '#theme_wrappers' => array('container__pager'),
      '#attributes' => array(
        'class' => array(
          'text-center',
        ),
      ),
      'pager' => array(
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array(
          'class' => array('pagination'),
        ),
      ),
    );
    return drupal_render($build);
  }
  return $output;
}

/**
 * Download document page
 */
function custom_document_download_page($node){
  if( $node->type != 'document' ){
    return drupal_not_found();
  }
  // dsm($node); 
  return theme('download_document_page', ['node' => $node]);
}

/**
 * Get path of file by id
 */
function _ajax_get_file_path($fid){
  if( is_numeric($fid) ){
    $file = file_load($fid);
    if( $file ){
      $_SESSION['download_token'] = drupal_random_key(25);
      $uri = $file->uri;
      $url = file_create_url($uri);
      drupal_json_output( $url.'?download_token='.$_SESSION['download_token'] );
      return;
    }
  }
  drupal_json_output('');
}


/**
 * Admin form
 */
function custom_import_pdf_form(){
  $form = array();
  $form['upload'] = array(
    '#type' => 'managed_file', 
    '#title' => t('Upload Excel file'),
    '#description' => t('Allowed extensions: xls, xlsx'),
    '#upload_location' => 'public://excel',
    '#required' => true,
    '#upload_validators' => array(
      'file_validate_extensions' => array('xls xlsx'),
      // Pass the maximum file size in bytes
      'file_validate_size' => array(parse_size(ini_get('upload_max_filesize'))),
    ),
  );
  $form['#submit'][] = 'custom_import_pdf_form_submit';
  return system_settings_form($form);
}

function custom_import_pdf_form_submit($form, $form_state){
  if( !empty($form_state['values']['upload']) ){
    $fid = $form_state['values']['upload'];
    $file = file_load($fid);
    if( $file ){
      // http://localhost:81/pdf/sites/default/files/demo.csv
      $dir = getcwd().'/sites/default/files/excel/';
      $url = file_create_url($file->uri);
      $data = _readExcelFile($dir.basename($url));
      if( count($data) ){
        $batch = array(
          'operations' => array(),
          'finished' => 'custom_import_document_batch_finished',
          'title' => t('Import Document'),
        );
        foreach ($data as $key => $value) {
          // custom_import_document_from_csv_batch_generate($value);
          $batch['operations'][] = array('custom_import_document_from_csv_batch_generate', [$value]);
        }
        batch_set($batch);
      }
    }
  }
  // dsm($form_state);
}


/**
 * Read Excel file, return to Array
 */
function _readExcelFile($fileUrl){
  require_once drupal_get_path('module', 'custom').'/vendor/PhpSpreadsheet/vendor/autoload.php';
  $spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($fileUrl);
  $data = [];
  if( $spreadsheet ){
    $sheetData = $spreadsheet->getActiveSheet()->toArray();
    if( count($sheetData) ){
      $header = $sheetData[0];
      for( $i = 1; $i < count($sheetData); $i++ ){
        $_arr = [];
        foreach ($sheetData[$i] as $key => $value) {
          $field = !empty($header[$key]) ? trim(strtolower($header[$key])) : 'unknown';
          $_arr[$field] = $value;
        }
        $data[] = $_arr;
      }
    }
  }
  return $data;
}


// function readCsvFile($file_url){
//   $data = array();
//   $file = fopen($file_url,"r") or exit('Không tìm thấy file cần mở! <a href="">Tải lại</a>');
//   //$data = feof($file);
//   $key = 0;
//   $header = array();
//   while(!feof($file)){
//     $row = fgetcsv($file, 0, ';');

//     if($key == 0){
//       $header = explode(';', trim($row[0]));
//       if( is_array($row) ){
//         $header = $row;
//       }
//     }
    
//     if($key > 0 && count($row)){
//       $temp = array();
//       foreach($header as $key1 => $value){
//         $value = trim(str_replace('"', '', $value));
//         $value = str_replace(';', '', $value);
//         $value = strtolower($value);
//         $temp[$value] = isset($row[$key1]) ? $row[$key1] : null;
//       }
//       $data[] = $temp;
//     }
//     $key++;
//   }
//   fclose($file);
//   return $data;
// }

function custom_import_document_from_csv_batch_generate($data, &$context){
  if( !empty($data['file name']) ){
    $title = $data['file name'];
  }
  else{
    return;
  }
  $keywords = !empty($data['keyword']) ? $data['keyword'] : '';
  $desc = !empty($data['description']) ? $data['description'] : '';
  $tags = !empty($data['tags']) ? explode(',', $data['tags']) : [];
  $brand = !empty($data['brand name']) ? $data['brand name'] : '';
  $categories = [];

  // dua danh sach danh muc vao mang
  for( $i = 1; $i <= 5; $i++ ){
    if( !empty($data['category '.$i]) ){
      $categories[] = $data['category '.$i];
    }
  }
  

  $nid = db_query('SELECT nid FROM {node} WHERE title = :mytitle', array(':mytitle' => $title))->fetchField();
  if( $nid ){
    $node = node_load($nid);
    $node->status = 1; /// publish if create new
  }
  else{
    /////////// Create node Document
    $node = new stdClass(); // We create a new node object
    $node->type = "document"; // Or any other content type you want
    $node->title = $title;
    $node->language = LANGUAGE_NONE; // Or any language code if Locale module is enabled.
    node_object_prepare($node); // Set some default values.
    $node->status = 0; /// unpublish if create new
  }

  if( count($tags) ){
    $node->field_tags[LANGUAGE_NONE] = [];
    foreach ($tags as $key => $value) {
      $value = trim($value);
      $tag_term = taxonomy_get_term_by_name($value, 'tags');
      $tag_term = reset($tag_term);
      if($tag_term){
        $node->field_tags[LANGUAGE_NONE][]['tid'] = $tag_term->tid;
      }
      else{
        $tag_term = new stdClass();
        $tag_term->name = $value;
        $tag_term->vid = 1;
        taxonomy_term_save($tag_term);
        $node->field_tags[LANGUAGE_NONE][]['tid'] = $tag_term->tid;
      }
    }
  }

  if( !empty($brand) ){
    $brand_term = taxonomy_get_term_by_name($brand, 'branch');
    $brand_term = reset($brand_term);
    if($brand_term){
      $node->field_branch[LANGUAGE_NONE][0]['tid'] = $brand_term->tid;
    }
    else{
      $brand_term = new stdClass();
      $brand_term->name = $brand;
      $brand_term->vid = 2;
      taxonomy_term_save($brand_term);
      $node->field_branch[LANGUAGE_NONE][0]['tid'] = $brand_term->tid;
    }
  }

  if( count($categories) ){
    $tid = _save_category_tree($categories);
    $node->field_categories[LANGUAGE_NONE][]['tid'] = $tid;
  }

  if( !empty($desc) ) $node->metatags[LANGUAGE_NONE]['description']['value'] = $desc;
  if( !empty($keywords) ) $node->metatags[LANGUAGE_NONE]['keywords']['value'] = $keywords;
  $node->body[LANGUAGE_NONE][0] = ['value' => $desc, 'format' => 'full_html'];

  node_save($node);
  if( $node->nid > 0 ){
    watchdog('Import Document', 'The document "%title" was imported', array('%title' => $title), WATCHDOG_NOTICE);
    if (!isset($context['results']['count'])) {
      $context['results']['count'] = 0;
    }
    $context['results']['count']++;
  }
  else{
    watchdog('Import Document', 'Something wrong! The document "%title" was not imported.', array('%title' => $title), WATCHDOG_NOTICE);
  }
}

/**
 * Save category
 */
function _save_category_tree($tree, $parent = 0){
  $tid = '';
  $tree = array_values($tree);
  $term_name = current($tree);
  if( count($tree) ) unset($tree[0]);

  $tid = db_query("SELECT taxonomy_term_data.tid AS tid
    FROM 
    {taxonomy_term_data} taxonomy_term_data
    LEFT JOIN {taxonomy_vocabulary} taxonomy_vocabulary ON taxonomy_term_data.vid = taxonomy_vocabulary.vid
    LEFT JOIN {taxonomy_term_hierarchy} taxonomy_term_hierarchy ON taxonomy_term_data.tid = taxonomy_term_hierarchy.tid
    WHERE (taxonomy_vocabulary.machine_name IN  ('categories') )
    AND (taxonomy_term_hierarchy.parent = '$parent')
    AND (taxonomy_term_data.name LIKE '$term_name')
    LIMIT 1 OFFSET 0")->fetchField();
  // dsm($tid);
  // return;
  if( $tid == null ){
    $term = new stdClass();
    $term->name = $term_name;
    $term->vid = 3;
    $term->parent = $parent;
    taxonomy_term_save($term);
    $tid = $term->tid;
  }
  if( count($tree) ) $tid = _save_category_tree($tree, $tid);
  return $tid;
}

function custom_private_download(){
  // Merge remainder of arguments from GET['q'], into relative file path.
  // var_dump( drupal_random_key(25)); exit();
  if( (isset($_SESSION['download_token'], $_GET['download_token']) && $_SESSION['download_token'] != $_GET['download_token'] )
      | empty($_GET['download_token']) 
      | empty($_SESSION['download_token']) ){
    drupal_access_denied();
    drupal_exit();
  }
  // dsm($_SESSION);
  $args = func_get_args();
  $scheme = array_shift($args);
  $target = implode('/', $args);
  $uri = $scheme . '://' . $target;
  if (file_stream_wrapper_valid_scheme($scheme) && file_exists($uri)) {
    $headers = file_download_headers($uri);
    if (count($headers)) {
      file_transfer($uri, $headers);
    }
    drupal_access_denied();
  }
  else {
    drupal_not_found();
  }
  drupal_exit();
}