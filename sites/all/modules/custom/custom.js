/* custom.js file */
(function($){
  // Handle product slideshow events
  Drupal.behaviors.custom_giaidieu = {
    attach: function (context, settings) {
      if($('#edit-field-key-search-value').length) {
        $('#edit-field-key-search-value').attr('placeholder', Drupal.t('Shop name or postcode'))
      }
    }
  };
})(jQuery);