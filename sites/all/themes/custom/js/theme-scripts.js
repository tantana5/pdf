window.$_GET = new URLSearchParams(location.search);
function resizeIframe(obj) {
	obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

(function($) {
	"use strict";

	Drupal.behaviors.custom_theme = {
  		attach: function (context, settings) {

  			////// Google reCaptcha form download
  			if( $('.download-area .captcha-form #gg-recaptcha').length ){
  				$('.download-area .captcha-form').on('submit', function(e){
  					$(this).find('.error').empty();
  					$(this).find('.result').empty();
  					var res = grecaptcha.getResponse(download_widget),
  					_this = $(this);
  					if (res == "" || res == undefined || res.length == 0){
						$(this).find('.error').html(Drupal.t('Wrong code. Try again!'));
						return false;
					}

					var docId = $(this).find('input[name="doc_id"]').val();
					if( docId != '' ){
						$.ajax({
							method: 'get',
							url: settings.basePath+'ajax/get-file-by-id/'+docId,
							success: function(result){
								console.log(result);
								if(result != ''){
									var timer = 14;
									_this.find('.result').html('<div class="count-down loading">'+(timer+1)+'</div>');
									var intervalDownload = setInterval(function(){
										if( timer >= 0 ){
											_this.find('.result>.count-down').text(timer);
										} else{
											clearInterval(intervalDownload);
											_this.find('.result').hide(300, function(e){
												$(this).html('<div style="font-size: 16px;"><span style="font-size:22px; font-weight: 700"><a href="'+ result +'" target="_blank">'+ Drupal.t('Download PDF') +'</a> | \
			                             		<a href="'+ result +'" target="_blank">'+ Drupal.t('View in browser') +'</a></span> <span style="font-size:13px">(not all browsers support it)</span></div>\
			                             		<span>'+ Drupal.t('right click on the link and choose "save target as" to download this manual') +'</span>');
												$(this).show();
											})
										}
										timer--;
									}, 1000);
									// _this.find('.result').html('<div style="font-size: 16px;"><span style="font-size:22px; font-weight: 700"><a href="'+ result +'" target="_blank">'+ Drupal.t('Download PDF') +'</a> | \
         //                     		<a href="'+ result +'" target="_blank">'+ Drupal.t('View in browser') +'</a></span> <span style="font-size:13px">(not all browsers support it)</span></div>\
         //                     		<span>'+ Drupal.t('right click on the link and choose "save target as" to download this manual') +'</span>');
									_this.find('#gg-recaptcha').hide();
									_this.find('button.btn').hide();
									_this.find('h4').hide();
								}
							},
							error: function(e){
								console.log(e);
							}
						})
					} else{
						$(this).find('.error').html(Drupal.t('This file does not exists. Please contact administrator!'));
					}
					
  					e.preventDefault();
  				})
  			}

  			//////////// Slide for thumbnail slide
  			if( $(".node-content .page-slide-show").length ){
  				$(".node-content .page-slide-show .slide-content").owlCarousel({
				    items: 10,
				    lazyLoad:true,
				    loop:true,
				    margin:10,
				    nav: true,
				    slideBy: 5,
				    navSpeed: 150,
				    startPosition: ( $_GET.get('page') != null ) ? $_GET.get('page') - 1 : null,
				});
  			}

  			//////////// Can chinh pdf khong bi xo lech, responsive
  			if( $('.doc-html .frame-doc>.frame-wrap').length ){
  				var parent_width = $('.doc-html .frame-doc').width(),
  				doc_width = $('.doc-html .frame-doc>.frame-wrap').width();
  				$('.doc-html .frame-doc').height( $('.doc-html .frame-doc').height()*parent_width/doc_width );
  				$('.doc-html .frame-doc>.frame-wrap').css({
  					transform: 'scale('+ parent_width/doc_width +')',
  					'transform-origin': 'top left'
  				});

  				$(window).resize(function(event) {
  					var parent_width = $('.doc-html .frame-doc').width(),
	  				doc_width = $('.doc-html .frame-doc>.frame-wrap').width();
	  				$('.doc-html .frame-doc>.frame-wrap').css({
	  					transform: 'scale('+ parent_width/doc_width +')',
	  					'transform-origin': 'top left'
	  				});
	  				$('.doc-html .frame-doc').height( $('.doc-html .frame-doc>.frame-wrap').height()*parent_width/doc_width );
  				});
  			}

		}
	};
})(jQuery);