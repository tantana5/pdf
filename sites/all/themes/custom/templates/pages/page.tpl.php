<div class="page-wrapper">
	<header class="header">
		<div class="container-fluid">
			<?php if($logo): ?>
				<a class="logo pull-left" href="<?php print $front_page ?>"><img src="<?php print $logo ?>"></a>
			<?php endif; ?>
			<?php if ($page['header']): ?>
				<?php print render($page['header']); ?>
			<?php endif; ?>
		</div>
	</header>

	<?php if ($messages): ?>
		<div id="messages"><div class="section clearfix">
			<?php print $messages; ?>
		</div></div> <!-- /.section, /#messages -->
	<?php endif; ?>

	<div class="content">
		<?php if( $breadcrumb ) print $breadcrumb; ?>
		<?php if($tabs) print render($tabs); ?>
		<?php if ($page['content']): ?>
			<?php print render($page['content']); ?>
		<?php endif; ?>
	</div>

	<footer class="footer">
		<?php if ($page['footer']): ?>
			<div class="container-\\">
				<?php print render($page['footer']); ?>
			</div>
		<?php endif; ?>
	</footer>
</div>