<div class="page-wrapper">
	<div class="header-home">
		<?php if ($page['header_front']): ?>
			<?php print render($page['header_front']); ?>
		<?php endif; ?>
	</div>
	<?php if ($messages): ?>
		<div id="messages"><div class="section clearfix">
			<?php print $messages; ?>
		</div></div> <!-- /.section, /#messages -->
	<?php endif; ?>
	<div class="content-home">
		<?php if ($page['content_front']): ?>
			<?php print render($page['content_front']); ?>
		<?php endif; ?>
	</div>
	<footer class="footer">
		<?php if ($page['footer']): ?>
			<div class="container-fluid">
				<?php print render($page['footer']); ?>
			</div>
		<?php endif; ?>
	</footer>
</div>