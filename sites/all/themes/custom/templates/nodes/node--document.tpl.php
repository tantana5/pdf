<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
// dsm($node->field_thumbs);
// dsm( variable_get('lazyloader_icon', '/sites/all/modules/lazyloader/loader/loader-7.gif'));
global $base_url;
$page = 1;
if( isset($_GET['page']) ) $page = (int)$_GET['page'];
$count_page = 0;
if( isset($node->field_content['und']) ) $count_page = count($node->field_content['und']);
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
	<div class="header-doc text-center">
		<div class="container">
			<ul class="option-menu margin0 padding0">
				<li><a href="<?php print $node_url; ?>/download" class="btn btn-success download"><i class="fa fa-download"></i> Download</a></li>
				<li><a href="#" class="btn btn-primary share"><i class="fa fa-share"></i> Share</a></li>
				<li><a href="#comments" class="btn btn-primary comment"><i class="fa fa-comments-o"></i> Comment</a></li>
			</ul>
			<form action="" class="form go-page-form inline-block">
				<span><?php print t('Page') ?></span>
				<input type="number" class="form-control" name="page" size="10">
				<span class="of"><?php print t('OF') ?></span>
				<span><?php if(!empty($node->field_thumbs['und'])) print ' '.count($node->field_thumbs['und']); ?></span>	
				<button type="submit" class="btn btn primary"><?php print t('Go') ?></button>
			</form>
			<div class="search-doc-form">
				<?php
				// $block = module_invoke('views', 'block_view', '-exp-document-page');
				// print render($block['content']);
	 			?>
 			</div>
		</div>
	</div>
	<div class="container">
		<div class="node-content">
			<div class="node-head">
				<?php print theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb())); ?>
				<h1 class="node-title"><?php print $title; ?></h1>
				<div class="body"><?php print truncate_utf8(render($content['body']), 255, true, true) ?></div>
				<div class="page-slide-show">
					<div class="slide-content owl-carousel owl-theme">
						<?php if(!empty($node->field_thumbs['und'])){
							foreach ($node->field_thumbs['und'] as $key => $value) { ?>
							<a class="item <?php print ( (!empty($_GET['page']) && $_GET['page'] == $key+1) | ( empty($_GET['page']) && $key == 0 ) ) ? 'active' : ''; ?>" href="<?php print $node_url.'?page='.($key+1) ?>" title="<?php print $title.' page '.($key+1) ?>">
								<span class="number"><?php print $key+1 ?></span>
								<img class="owl-lazy" data-src="<?php print image_style_url('image_crop_120x150', $value['uri']) ?>" alt="<?php print $title.' page '.($key+1) ?>">
							</a>
						<?php }
						} ?>
					</div>
				</div>
			</div>

			<div class="doc-html">
				<div class="doc-nav">
					<?php if( $page > 1 && $page <= $count_page  ): ?>
						<a href="<?php print $node_url.'?page='.($page-1) ?>" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
					<?php endif; ?>
					<?php if( $page > 0 && $page < $count_page  ): ?>
						<a href="<?php print $node_url.'?page='.($page+1) ?>" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
					<?php endif; ?>
				</div>
				<?php if( !empty($page) && $page > 0 && $page <= $count_page && !empty($node->field_content['und'][$page-1]['uri']) ): ?>
					<?php
					print _filter_dom_doc_html($node);
					?>
				<?php else: ?>
					<div class="alert alert-warning" role="alert">
						<?php print t('The requested page could not be found!'); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="doc-pagging">
				<?php print _custom_pager( ['total' => $count_page, 'current' => $page, 'quantity' => 7 ] ); ?>
			</div>
		</div> <!-- End node-content -->
	</div> <!-- End container -->
	
	<div class="relate-doc">
		<div class="container">
			<h2 class="block-title"><?php print t('Relate document'); ?></h2>
			<?php print views_embed_view('document', 'block_4'); ?>
		</div>
	</div>

	<div class="doc-comment">
		<div class="container">
			<div class="col-sm-10 col-sm-push-1">
				<h2 class="title text-center"><?php print t('Comments to this Document') ?></h2>
				<?php print render($content['comments']); ?>
			</div>
		</div>
	</div>
	
</div>