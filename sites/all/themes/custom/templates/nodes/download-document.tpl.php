<?php // dsm($node); ?>
<div class="download-header">
	<?php print theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb())); ?>
	<h1 class="download-title"><?php print t('Download document') ?></h1>
</div> <!-- End download header -->
<div class="container-fluid">
	<div class="download-area">
		<h2><?php print t('Download').' '.$node->title ?></h2>
		<div class="desc">
			<?php print $node->title ?> | <?php print t('Brand') ?>: | <?php print t('Category') ?>: | 
			<?php print t('Size') ?>: <?php if( !empty($node->field_file['und'][0]['filesize']) ){ print format_size($node->field_file['und'][0]['filesize']); } ?> | <?php print t('Pages') ?>: <?php print !empty($node->field_content['und']) ? count($node->field_content['und']) : '' ?><br>
			<?php print t('This document also for').': ' ?>
		</div>
		<div class="captcha-form">
			<script type="text/javascript">
				var download_widget;
				var onloadCallback = function() {
				    download_widget = grecaptcha.render('gg-recaptcha', {
						'sitekey' : '<?php print variable_get('google_captcha_site_key', '6LeorD4UAAAAABhVtNhZkp5b-9E-zSyOa6v89qsw') ?>',
						'theme' : 'light'
				    });
				};
			</script>
			<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit'></script>
			<form action="?" method="POST">
				<h4><?php print t('Please, tick the box below to get your link') ?>:</h4>
				<input type="hidden" name="doc_id" value="<?php print !empty($node->field_file['und'][0]['fid']) ? $node->field_file['und'][0]['fid'] : '' ?>">
				<div class="error"></div>
				<div class="result"></div>
				<div id="gg-recaptcha"></div>
				<button type="submit" class="btn btn-primary" id="get-link-download"><i class="fa fa-download"></i> Get document</button>
			</form>
		</div>
	</div> <!-- End download area -->
</div>